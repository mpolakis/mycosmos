# The MIT License (MIT)

# Copyright (c) 2014 Michalis Polakis

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# MyCosmos.gr sms script
# Usage : ruby sms.rb USERNAME PASSWORD RECIPIENT_NUMBER MESSAGE
# Example : ruby sms.rb 697xxxxxxx xyzxyzxyz 6978xxxxxx 'test message'
# You can also add an alias like : alias sms='ruby ~michalis/workspace/sms/sms.rb 697xxxxxxx@mycosmos.gr password '
# and then use it like > sms recipient_number message


require "rubygems"
require "capybara"
include Capybara::DSL

Capybara.current_driver = :selenium
Capybara.run_server = false
Capybara.app_host = "https://www.mycosmos.gr"

visit "/"
within(".si-text-body") do
  fill_in "Email",    with: ARGV[0] # Username
  fill_in "Password", with: ARGV[1] # Password
end
click_button "Login"

# Navigate away from modal notifying user to transfer your old emails
e = find_button("Cancel") rescue nil
unless e.nil?
  click_button "Cancel"
end

click_link "rcmbtn102" # go to web2sms

within_frame("preferences-frame") do
  within("#preferences-details") do
    fill_in "_to",      with: ARGV[2] # Recipient
    fill_in "_message", with: ARGV[3] # SMS Message
  end
  click_button "Send"
end
